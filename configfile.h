#ifndef _CONFIG_H_
#define _CONFIG_H_

int config_parse(const char *config);
void config_free(void);

const char * config_get_string(const char *section_str, const char *option, const char *def);

int config_get_int(const char *section, const char *option, int *value, int def);

int config_get_strings(const char *section_str, const char *option,
			int (*callback)(const char *value, void *privdata),
			void *privdata);

struct strtoken {
	const char *input;
	const char *delim;
	int maxfields;
	int count;
	char *field[0];
};

struct strtoken * strtokenize(const char *input, const char *delim, int maxfields);

struct strtoken * config_get_strtoken(const char *section_str, const char *option, const char *delim, int maxfields);

int config_get_strtokens(const char *section_str, const char *option, const char *delim, int maxfields,
			int (*callback)(struct strtoken *tokens, void *privdata),
			void *privdata);

#endif /* _CONFIG_H_ */
