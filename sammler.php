<?php

define('CONFIG', '%%WWW_CONFIG%%');
define('BASE_DIR', '%%DATA_DIR%%');
define('RRDTOOL', '/usr/bin/rrdtool');

function setup_html($conf) {
	echo "<html><head><title>sammler graph setup</title>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-15\"/>\n";
	echo "</head><body>\n";
	echo "<form method=\"POST\" action=\"{$_SERVER['SCRIPT_NAME']}?action=setup\">\n";
	echo "<table>\n";

	echo "<tr><td><b>Graph Height:</b></td>\n";
	echo "<td><input type=\"text\" name=\"height\" value=\"{$conf['height']}\"></td></tr>\n";
	echo "<tr><td><b>Graph Width:</b></td>\n";
	echo "<td><input type=\"text\" name=\"width\" value=\"{$conf['width']}\"></td></tr>\n";
	echo "<tr><td colspan=\"4\"><hr></td></tr>\n";

	echo "<tr><td><b>Available Views:</b></td><td colspan=\"2\"></td><td><b>default</b></td></tr>\n";
	foreach ($conf['views'] as $name => $value) {
		echo "<tr><td>({$value}s)</td>\n";
		echo "<td><input type=\"text\" name=\"views[$value]\" value=\"{$name}\"></td><td></td>\n";
		if ($conf['default_view'] == $value)
			echo "<td><input type=\"radio\" name=\"default_view\" value=\"{$value}\" checked=\"checked\"></td>\n";
		else
			echo "<td><input type=\"radio\" name=\"default_view\" value=\"{$value}\"></td>\n";

		echo "</tr>\n";
	}

	echo "<tr><td><b>add view:</b></td><td><input type=\"text\" name=\"views[new]\"></td></tr>\n";

	echo "<tr><td colspan=\"4\"><hr></td></tr>\n";
	foreach ($conf['hosts'] as $hostid => $host) {
		echo "<tr><td><b>Hostname:</b></td>\n";
		echo "<td><input type=\"text\" name=\"hosts[{$hostid}][hostname]\" value=\"{$host['hostname']}\"></td>\n";

		echo "<td><a href=\"{$_SERVER['SCRIPT_NAME']}?action=setup&sub=move_host_up&host={$hostid}\">up</a>&nbsp;\n";
		echo "<a href=\"{$_SERVER['SCRIPT_NAME']}?action=setup&sub=move_host_down&host={$hostid}\">down</a></td>\n";

		if ($host['show'])
			echo "<td><input type=\"checkbox\" name=\"hosts[{$hostid}][show]\" checked=\"checked\">show</td>";
		else
			echo "<td><input type=\"checkbox\" name=\"hosts[{$hostid}][show]\">show</td>";

		echo "</tr>\n";

		foreach ($host['rrds'] as $rrdid => $rrd) {
			echo "<tr><td>&nbsp;&nbsp;{$rrd['rrd']}</td>\n";
			echo "<td><input type=\"text\" name=\"hosts[{$hostid}][rrds][{$rrdid}][title]\" value=\"{$rrd['title']}\"></td>\n";

			echo "<td><a href=\"{$_SERVER['SCRIPT_NAME']}?action=setup&sub=move_rrd_up&host={$hostid}&rrd={$rrdid}\">up</a>&nbsp;\n";
			echo "<a href=\"{$_SERVER['SCRIPT_NAME']}?action=setup&sub=move_rrd_down&host={$hostid}&rrd={$rrdid}\">down</a></td>\n";

			if ($rrd['show'])
				echo "<td><input type=\"checkbox\" name=\"hosts[{$hostid}][rrds][{$rrdid}][show]\" checked=\"checked\">show</td>";
			else
				echo "<td><input type=\"checkbox\" name=\"hosts[{$hostid}][rrds][{$rrdid}][show]\">show</td>";

			echo "</tr>\n";
		}

		echo "<tr><td colspan=\"4\"><hr></td></tr>\n";
	}

	echo "<tr><td colspan=\"4\" align=\"center\">\n";
	echo "<input type=\"button\" value=\"Show\" onClick=\"javascript:window.location.href='{$_SERVER['SCRIPT_NAME']}?action=show'\">&nbsp\n";
	echo "<input type=\"submit\" value=\"Save Config\">&nbsp\n";
	echo "<input type=\"button\" value=\"Rebuild Config\" onClick=\"javascript:window.location.href='{$_SERVER['SCRIPT_NAME']}?action=init'\">\n";
	echo "</td></tr>\n";
	echo "</table></form>\n";
	echo "</body></html>\n";
}

function show_html($conf) {
	echo "<html><head><title>Stats</title>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-15\"/>\n";
	echo "</head><body>\n";

	$hostid = isset($_GET['host']) ? $_GET['host'] : 0;
	$view = isset($_GET['view']) ? $_GET['view'] : $conf['default_view'];

	echo "<b>Select Host:</b>\n";
	echo "<select name=\"host\" OnChange=\"document.location.href='{$_SERVER['SCRIPT_NAME']}?action=show&host='+this.value+'&view={$view}'\">\n";
	foreach ($conf['hosts'] as $id => $host) {
		if (!$host['show'])
			continue;

		if ($hostid == $id)
			echo "<option value=\"{$id}\" selected=\"selected\">{$host['hostname']}</option>\n";
		else
			echo "<option value=\"{$id}\">{$host['hostname']}</option>\n";
	}
	echo "</select>&nbsp;\n";

	echo "<td><select name=\"view\" OnChange=\"document.location.href='{$_SERVER['SCRIPT_NAME']}?action=show&host={$hostid}&view='+this.value\">\n";
	foreach ($conf['views'] as $name => $value) {
		if ($view == $value)
			echo "<option value=\"{$value}\" selected=\"selected\">{$name}</option>\n";
		else
			echo "<option value=\"{$value}\">{$name}</option>\n";
	}
	echo "</select><br><br>\n";


	foreach ($conf['hosts'][$hostid]['rrds'] as $rrdid => $rrd) {
		if (!$rrd['show'])
			continue;

		echo "<img src=\"{$_SERVER['SCRIPT_NAME']}?action=rrd&host={$hostid}&rrd={$rrdid}&view={$view}\" border=\"0\"><br><br>\n";
	}

	echo "</body></html>\n";
}


function read_config($filename) {
	$handle = @fopen($filename, "r");
	if ($handle !== false && filesize($filename) > 0) {
		$contents = fread($handle, filesize($filename));
		fclose($handle);

		return unserialize($contents);
	}
}

function write_config($filename, $config) {
	$handle = fopen($filename, "w+");
	if ($handle !== false) {
		$contents = serialize($config);
		fwrite($handle, $contents);
		fclose($handle);
	}
}

function get_hostdirs() {
	$retval = array();
	$dh = opendir(BASE_DIR);
	while (($file = readdir($dh)) !== false)
		if (is_dir(BASE_DIR.$file) && $file != "." && $file != "..")
			$retval[] = $file;

	closedir($dh);
	natsort($retval);
	return $retval;
}

function get_rrdfiles($directory) {
	$retval = array();
	$dh = opendir(BASE_DIR.$directory);
	while (($file = readdir($dh)) !== false) {
		if (is_dir(BASE_DIR.$file))
			continue;

		if (strstr($file, "rrd") !== false)
			$retval[] = $file;
	}
	closedir($dh);
	natsort($retval);
	return $retval;
}

function get_rrd_type($filename) {
	$types = array_flip(array(
		"cpu",
		"load",
		"memory",
		"swap",
		"proc",
		"vmstat",
		"random",
		"uptime",
		"hwmon",
		"alixusv",
		"net",
		"rtstat",
		"rtcache",
		"ctstat",
		"conntrack",
		"disk",
		"mount",
		"apache",
		"mysql",
		"ts2"
	));

	$tmp1 = explode('-', $filename);
	$tmp2 = explode('_', $tmp1[0]);
	$type = $tmp2[0];

	if (isset($types[$type]))
		return $types[$type];

	return $filename;
}

function rrd_sort($a, $b) {
	if (!is_numeric($a['type']) && !is_numeric($b['type']))
		return strcmp($a['type'], $b['type']);

	if (is_numeric($a['type']) && !is_numeric($b['type']))
		return -1;

	if (!is_numeric($a['type']) && is_numeric($b['type']))
		return +1;

	if ($a['type'] > $b['type'])
		return +1;

	if ($a['type'] < $b['type'])
		return -1;

	return strcmp($a['title'], $b['title']);
}

function create_config() {
	$conf = array();
	$conf['default_view'] = 86400;
	$conf['views'] = array("1 hour" => 3600,
				"6 hours" => 21600,
				"1 day" => 86400,
				"1 week" => 604800,
				"1 month" => 2678400);
	$conf['height'] = 120;
	$conf['width'] = 800;
	$conf['hosts'] = array();

	$hosts = get_hostdirs();
	foreach ($hosts as $host) {
		$rrds = array();
		$tmp = get_rrdfiles($host);
		foreach ($tmp as $file) {
			$basename = str_replace(".rrd", "", basename($file));
			$rrds[] = array(
				'show' => true,
				'title' => $basename,
				'rrd' => "$host/$file",
				'type' => get_rrd_type($basename),
				);
		}

		uasort($rrds, rrd_sort);

		$conf['hosts'][] = array(
			'hostname' => $host,
			'show' => true,
			'rrds' => $rrds,
			);
	}

	return $conf;
}

function setup($conf) {
	if (isset($_GET['sub']) && $_GET['sub'] == "move_host_up") {
		$hostid = isset($_GET['host']) ? $_GET['host'] : -1;
		if (isset($conf['hosts'][$hostid]) && isset($conf['hosts'][$hostid -1])) {
			$a = $conf['hosts'][$hostid];
			$b = $conf['hosts'][$hostid -1];

			$conf['hosts'][$hostid] = $b;
			$conf['hosts'][$hostid -1] = $a;
		}
		return $conf;

	} else if (isset($_GET['sub']) && $_GET['sub'] == "move_host_down") {
		$hostid = isset($_GET['host']) ? $_GET['host'] : -1;
		if (isset($conf['hosts'][$hostid]) && isset($conf['hosts'][$hostid +1])) {
			$a = $conf['hosts'][$hostid];
			$b = $conf['hosts'][$hostid +1];

			$conf['hosts'][$hostid] = $b;
			$conf['hosts'][$hostid +1] = $a;
		}
		return $conf;

	} else if (isset($_GET['sub']) && $_GET['sub'] == "move_rrd_up") {
		$hostid = isset($_GET['host']) ? $_GET['host'] : -1;
		$rrdid = isset($_GET['rrd']) ? $_GET['rrd'] : -1;
		if (isset($conf['hosts'][$hostid]['rrds'][$rrdid]) && isset($conf['hosts'][$hostid]['rrds'][$rrdid -1])) {
			$a = $conf['hosts'][$hostid]['rrds'][$rrdid];
			$b = $conf['hosts'][$hostid]['rrds'][$rrdid -1];

			$conf['hosts'][$hostid]['rrds'][$rrdid] = $b;
			$conf['hosts'][$hostid]['rrds'][$rrdid -1] = $a;
		}
		return $conf;

	} else if (isset($_GET['sub']) && $_GET['sub'] == "move_rrd_down") {
		$hostid = isset($_GET['host']) ? $_GET['host'] : -1;
		$rrdid = isset($_GET['rrd']) ? $_GET['rrd'] : -1;
		if (isset($conf['hosts'][$hostid]['rrds'][$rrdid]) && isset($conf['hosts'][$hostid]['rrds'][$rrdid +1])) {
			$a = $conf['hosts'][$hostid]['rrds'][$rrdid];
			$b = $conf['hosts'][$hostid]['rrds'][$rrdid +1];

			$conf['hosts'][$hostid]['rrds'][$rrdid] = $b;
			$conf['hosts'][$hostid]['rrds'][$rrdid +1] = $a;
		}
		return $conf;

	} else if (!empty($_POST)) {
		$conf['default_view'] = $_POST['default_view'];
		$conf['height'] = $_POST['height'];
		$conf['width'] = $_POST['width'];

		$conf['views'] = array();
		foreach ($_POST['views'] as $name) {
			if (empty($name))
				continue;

			$tmp = strtotime($name);
			if ($tmp != -1)
				$conf['views'][$name] = $tmp - time();
		}
		natsort($conf['views']);

		foreach ($_POST['hosts'] as $hostid => $host) {
			$conf['hosts'][$hostid]['hostname'] = $host['hostname'];
			$conf['hosts'][$hostid]['show'] = ($host['show'] == "on");

			foreach ($host['rrds'] as $rrdid => $rrd) {
				$conf['hosts'][$hostid]['rrds'][$rrdid]['title'] = $_POST['hosts'][$hostid]['rrds'][$rrdid]['title'];
				$conf['hosts'][$hostid]['rrds'][$rrdid]['show'] = ($_POST['hosts'][$hostid]['rrds'][$rrdid]['show'] == "on");
			}
		}
		return $conf;

	} else {
		setup_html($conf);
	}
}

function show_rrd($conf) {
	$hostid = isset($_GET['host']) ? $_GET['host'] : -1;
	$rrdid = isset($_GET['rrd']) ? $_GET['rrd'] : -1;

	if (!isset($conf['hosts'][$hostid]['rrds'][$rrdid]['rrd']))
		return;

	$view = isset($_GET['view']) ? -$_GET['view'] : -$conf['default_view'];
	$rrdfile = BASE_DIR.$conf['hosts'][$hostid]['rrds'][$rrdid]['rrd'];
	$title = $conf['hosts'][$hostid]['rrds'][$rrdid]['title'];
	$height = $conf['height'];
	$width = $conf['width'];

	$cmd = RRDTOOL." graph - --imgformat PNG --start {$view} --end -10 --title \"{$title}\" --rigid ";

	$tmp1 = explode('-', basename($rrdfile));
	$tmp2 = explode('_', $tmp1[0]);
	$tmp3 = explode('.', $tmp2[0]);
	switch ($tmp3[0]) {
	case 'alixusv':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale --vertical-label=\"Voltage\" ".
			"DEF:a={$rrdfile}:ibat:AVERAGE ".
			"DEF:b={$rrdfile}:ubat:AVERAGE ".
			"DEF:c={$rrdfile}:uin:AVERAGE ".
			'CDEF:aa=a,50,/ '.
			'CDEF:bb=b,1000,/ '.
			'CDEF:cc=c,1000,/ '.
			'CDEF:perr=aa,UN,INF,UNKN,IF CDEF:nerr=aa,UN,-INF,UNKN,IF '.
			'HRULE:0#808080 AREA:perr#FFD0D0 AREA:nerr#FFD0D0 '.
			'LINE2:aa#FF0000:"Ibat " GPRINT:a:LAST:"Current\:%7.0lf%smA" GPRINT:a:MIN:"Minimum\:%7.0lf%smA" GPRINT:a:MAX:"Maximum\:%7.0lf%smA\n" '.
			'LINE2:bb#00CF00:"Ubat " GPRINT:bb:LAST:"Current\:%7.3lf%s V" GPRINT:bb:MIN:"Minimum\:%7.3lf%s V" GPRINT:bb:MAX:"Maximum\:%7.3lf%s V\n" '.
			'LINE2:cc#0000FF:"Uin  " GPRINT:cc:LAST:"Current\:%7.3lf%s V" GPRINT:cc:MIN:"Minimum\:%7.3lf%s V" GPRINT:cc:MAX:"Maximum\:%7.3lf%s V\n" '.
			'';
		break;

	case 'apache':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"Workers\" ".
			"DEF:aa={$rrdfile}:total_accesses:AVERAGE ".
			"DEF:b={$rrdfile}:total_kbytes:AVERAGE ".
			"DEF:c={$rrdfile}:busy_workers:AVERAGE ".
			"DEF:d={$rrdfile}:idle_workers:AVERAGE ".
			'CDEF:a=aa,60,* '.
			'CDEF:err=aa,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:c#FF0000:"Busy Workers" GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:d#00CF00:"Idle Workers" GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE2:a#0000FF:"Accesses    " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'';
		break;

	case 'cpu':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"percent\" ".
			"DEF:a={$rrdfile}:user:AVERAGE ".
			"DEF:b={$rrdfile}:nice:AVERAGE ".
			"DEF:c={$rrdfile}:system:AVERAGE ".
			"DEF:e={$rrdfile}:iowait:AVERAGE ".
			"DEF:f={$rrdfile}:irq:AVERAGE ".
			"DEF:g={$rrdfile}:softirq:AVERAGE ".
			"DEF:h={$rrdfile}:steal:AVERAGE ".
			"DEF:amax={$rrdfile}:user:MAX ".
			"DEF:bmax={$rrdfile}:nice:MAX ".
			"DEF:cmax={$rrdfile}:system:MAX ".
			"DEF:emax={$rrdfile}:iowait:MAX ".
			"DEF:fmax={$rrdfile}:irq:MAX ".
			"DEF:gmax={$rrdfile}:softirq:MAX ".
			"DEF:hmax={$rrdfile}:steal:MAX ".
			"CDEF:allmax=amax,bmax,cmax,emax,fmax,gmax,hmax,+,+,+,+,+,+ ".
			'CDEF:err=allmax,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:allmax#C0C0C0 LINE1:allmax#808080 '.
			'AREA:c#FF0000:"System  " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:f#EA8F00:"IRQ     " GPRINT:f:LAST:"Current\:%8.2lf %s" GPRINT:f:AVERAGE:"Average\:%8.2lf %s" GPRINT:f:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:g#FFFF00:"Soft-IRQ" GPRINT:g:LAST:"Current\:%8.2lf %s" GPRINT:g:AVERAGE:"Average\:%8.2lf %s" GPRINT:g:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:h#0040FF:"IO-Wait " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:a#00CFCF:"User    " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"Nice    " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:h#FF00FF:"Stolen  " GPRINT:h:LAST:"Current\:%8.2lf %s" GPRINT:h:AVERAGE:"Average\:%8.2lf %s" GPRINT:h:MAX:"Maximum\:%8.2lf %s\n" ';
			break;

	case 'conntrack':
		$tmp3 = explode('.', $tmp1[1]);
		switch ($tmp3[0]) {
		case 'proto':
			$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"Connections\" ".
				"DEF:a={$rrdfile}:tcp:AVERAGE ".
				"DEF:b={$rrdfile}:udp:AVERAGE ".
				"DEF:c={$rrdfile}:unknown:AVERAGE ".
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'AREA:a#00CF00:"tcp    " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:b#0000FF:"udp    " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:c#FF0000:"unknown" GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" ';
			break;

		case 'tcp':
			$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"TCP Connections\" ".
				"DEF:a={$rrdfile}:none:AVERAGE ".
				"DEF:b={$rrdfile}:syn_sent:AVERAGE ".
				"DEF:c={$rrdfile}:syn_recv:AVERAGE ".
				"DEF:d={$rrdfile}:established:AVERAGE ".
				"DEF:e={$rrdfile}:fin_wait:AVERAGE ".
				"DEF:f={$rrdfile}:close_wait:AVERAGE ".
				"DEF:g={$rrdfile}:last_ack:AVERAGE ".
				"DEF:h={$rrdfile}:time_wait:AVERAGE ".
				"DEF:i={$rrdfile}:close:AVERAGE ".
				"DEF:j={$rrdfile}:listen:AVERAGE ".
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'AREA:b#FF0000:"SYN_SEND   " GPRINT:b:LAST:"Current\:%8.2lf" GPRINT:b:AVERAGE:"Average\:%8.2lf" GPRINT:b:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:f#FF00FF:"CLOSE_WAIT " GPRINT:f:LAST:"Current\:%8.2lf" GPRINT:f:AVERAGE:"Average\:%8.2lf" GPRINT:f:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:g#0000FF:"LAST_ACK   " GPRINT:g:LAST:"Current\:%8.2lf" GPRINT:g:AVERAGE:"Average\:%8.2lf" GPRINT:g:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:h#007FFF:"TIME_WAIT  " GPRINT:h:LAST:"Current\:%8.2lf" GPRINT:h:AVERAGE:"Average\:%8.2lf" GPRINT:h:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:c#00FFFF:"SYN_RECV   " GPRINT:c:LAST:"Current\:%8.2lf" GPRINT:c:AVERAGE:"Average\:%8.2lf" GPRINT:c:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:d#00FF00:"ESTABLISHED" GPRINT:d:LAST:"Current\:%8.2lf" GPRINT:d:AVERAGE:"Average\:%8.2lf" GPRINT:d:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:e#FFFF00:"FIN_WAIT   " GPRINT:e:LAST:"Current\:%8.2lf" GPRINT:e:AVERAGE:"Average\:%8.2lf" GPRINT:e:MAX:"Maximum\:%8.2lf\n" '.
				'STACK:i#FF7F00:"CLOSE      " GPRINT:i:LAST:"Current\:%8.2lf" GPRINT:i:AVERAGE:"Average\:%8.2lf" GPRINT:i:MAX:"Maximum\:%8.2lf\n" ';
			break;

		default:
			$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"Connections\" ".
				"DEF:a={$rrdfile}:total:AVERAGE ".
				"DEF:b={$rrdfile}:local:AVERAGE ".
				"DEF:c={$rrdfile}:nated:AVERAGE ".
				"DEF:d={$rrdfile}:unreplied:AVERAGE ".
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'LINE1:a#000000:"Total    " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'AREA:b#00CF00:"Local    " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:c#0000FF:"NATed    " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:d#FF0000:"Unreplied" GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" ';
				break;
		}
		break;

	case 'ctstat':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"calls\" ".
			"DEF:a={$rrdfile}:searched:AVERAGE ".
			"DEF:b={$rrdfile}:found:AVERAGE ".
			"DEF:c={$rrdfile}:new:AVERAGE ".
			"DEF:d={$rrdfile}:invalid:AVERAGE ".
			"DEF:e={$rrdfile}:ignore:AVERAGE ".
			"DEF:f={$rrdfile}:delete:AVERAGE ".
			"DEF:g={$rrdfile}:delete_list:AVERAGE ".
			"DEF:h={$rrdfile}:insert:AVERAGE ".
			"DEF:i={$rrdfile}:insert_failed:AVERAGE ".
			"DEF:j={$rrdfile}:drop:AVERAGE ".
			"DEF:k={$rrdfile}:early_drop:AVERAGE ".
			"DEF:l={$rrdfile}:icmp_error:AVERAGE ".
			"DEF:m={$rrdfile}:expect_new:AVERAGE ".
			"DEF:n={$rrdfile}:expect_create:AVERAGE ".
			"DEF:o={$rrdfile}:expect_delete:AVERAGE ".
			'CDEF:ii=i,-1,* '.
			'CDEF:jj=j,-1,* '.
			'CDEF:kk=k,-1,* '.
			'CDEF:ll=l,-1,* '.
			'CDEF:mm=m,-1,* '.
			'CDEF:nn=n,-1,* '.
			'CDEF:oo=o,-1,* '.
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'LINE1:a#FF0000:"searched      " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:b#FF00FF:"found         " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:c#0000FF:"new           " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:d#007FFF:"invalid       " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:e#00FFFF:"ignore        " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:f#00FF00:"delete        " GPRINT:f:LAST:"Current\:%8.0lf" GPRINT:f:AVERAGE:"Average\:%8.0lf" GPRINT:f:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:g#FFFF00:"delete_list   " GPRINT:g:LAST:"Current\:%8.0lf" GPRINT:g:AVERAGE:"Average\:%8.0lf" GPRINT:g:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:h#FF7F00:"insert        " GPRINT:h:LAST:"Current\:%8.0lf" GPRINT:h:AVERAGE:"Average\:%8.0lf" GPRINT:h:MAX:"Maximum\:%8.0lf\n" '.
			'HRULE:0#000000:"-\n" '.
			'LINE1:ii#FF0000:"insert_failed " GPRINT:i:LAST:"Current\:%8.0lf" GPRINT:i:AVERAGE:"Average\:%8.0lf" GPRINT:i:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:jj#FF00FF:"drop          " GPRINT:j:LAST:"Current\:%8.0lf" GPRINT:j:AVERAGE:"Average\:%8.0lf" GPRINT:j:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:kk#0000FF:"early_drop    " GPRINT:k:LAST:"Current\:%8.0lf" GPRINT:k:AVERAGE:"Average\:%8.0lf" GPRINT:k:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:ll#007FFF:"icmp_errore   " GPRINT:l:LAST:"Current\:%8.0lf" GPRINT:l:AVERAGE:"Average\:%8.0lf" GPRINT:l:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:mm#00FFFF:"expect_new    " GPRINT:m:LAST:"Current\:%8.0lf" GPRINT:m:AVERAGE:"Average\:%8.0lf" GPRINT:m:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:nn#00FF00:"expect_create " GPRINT:n:LAST:"Current\:%8.0lf" GPRINT:n:AVERAGE:"Average\:%8.0lf" GPRINT:n:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:oo#FFFF00:"expect_delete " GPRINT:o:LAST:"Current\:%8.0lf" GPRINT:o:AVERAGE:"Average\:%8.0lf" GPRINT:o:MAX:"Maximum\:%8.0lf\n" ';
		break;

	case 'disk':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"sectors\" ".
			"DEF:a={$rrdfile}:read_cnt:AVERAGE ".
			"DEF:b={$rrdfile}:read_sec:AVERAGE ".
			"DEF:c={$rrdfile}:read_ms:AVERAGE ".
			"DEF:dd={$rrdfile}:write_cnt:AVERAGE ".
			"DEF:ee={$rrdfile}:write_cnt:AVERAGE ".
			"DEF:ff={$rrdfile}:write_ms:AVERAGE ".
			'CDEF:d=dd,-1,* '.
			'CDEF:e=ee,-1,* '.
			'CDEF:f=ff,-1,* '.
			'CDEF:oerr=a,UN,INF,UNKN,IF CDEF:ierr=dd,UN,-INF,UNKN,IF '.
			'HRULE:0#FF0000 AREA:ierr#FFD0D0 AREA:oerr#FFD0D0 '.
			'LINE1:a#FF0000:"read_count    " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:b#FF00FF:"read_sectors  " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:c#0000FF:"sectors / ms  " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:d#FF0000:"write_count   " GPRINT:dd:LAST:"Current\:%8.0lf" GPRINT:dd:AVERAGE:"Average\:%8.0lf" GPRINT:dd:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:e#FF00FF:"write_sectors " GPRINT:ee:LAST:"Current\:%8.0lf" GPRINT:ee:AVERAGE:"Average\:%8.0lf" GPRINT:ee:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:f#0000FF:"writes / ms   " GPRINT:ff:LAST:"Current\:%8.0lf" GPRINT:ff:AVERAGE:"Average\:%8.0lf" GPRINT:ff:MAX:"Maximum\:%8.0lf\n" '.
			'';
		break;

	case 'diskstandby':
		$height /= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"Active\" ".
			"DEF:a={$rrdfile}:active:AVERAGE ".
			"CDEF:b=1,a,- ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:a#FF0000:"Active\n" '.
			'STACK:b#00CF00:"Standby" ';
		break;

	case 'hddtemp':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"Temperature\" ".
			"DEF:a={$rrdfile}:temperature:AVERAGE ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:a#00CF00:"Temperature" GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#404040 ';
		break;

	case 'hwmon':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"Temperature\" ".
			"DEF:a={$rrdfile}:temp:AVERAGE ".
			"CDEF:aa=a,1000,/ ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:aa#00CF00:"Temperature" GPRINT:aa:LAST:"Current\:%8.2lf %s" GPRINT:aa:AVERAGE:"Average\:%8.2lf %s" GPRINT:aa:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:aa#404040 ';
		break;

	case 'load':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"load\" ".
			"DEF:a={$rrdfile}:1min:MAX ".
			"DEF:b={$rrdfile}:5min:MAX ".
			"DEF:c={$rrdfile}:15min:MAX ".
			'CDEF:x=a,b,c,MAX,MAX '.
			'CDEF:err=x,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:a#EACC00:" 1 Minute Maximum " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:b#EA8F00:" 5 Minute Maximum " GPRINT:b:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:c#FF0000:"15 Minute Maximum " GPRINT:c:LAST:"Current\:%8.2lf %s\n" '.
			'LINE1:x#404040:"Total"';
		break;

	case 'memory':
		$cmd .= "--base=1024 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"kB\" ".
			"DEF:aa={$rrdfile}:total:AVERAGE ".
			"DEF:bb={$rrdfile}:free:AVERAGE ".
			"DEF:cc={$rrdfile}:buffers:AVERAGE ".
			"DEF:dd={$rrdfile}:cached:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=bb,1024,* '.
			'CDEF:c=cc,1024,* '.
			'CDEF:d=dd,1024,* '.
			'CDEF:x=aa,bb,cc,dd,+,+,-,1024,* '.
			'CDEF:xc=x,c,+ '.
			'CDEF:xcd=x,c,d,+,+ '.
			'CDEF:err=xcd,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'LINE1:a#000000:"Total Memory  " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:x#FF0000:"Used Memory   " GPRINT:x:LAST:"Current\:%8.2lf %s" GPRINT:x:AVERAGE:"Average\:%8.2lf %s" GPRINT:x:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:c#FF7D00:"Buffer Memory " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:d#FFC73B:"Cache Memory  " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"Free Memory   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:x#404040 LINE1:xc#404040 LINE1:xcd#404040';
		break;

	case 'mount':
		$cmd .= "--base=1024 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"kB\" ".
			"DEF:aa={$rrdfile}:block_total:AVERAGE ".
			"DEF:cc={$rrdfile}:block_free:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=aa,cc,-,1024,* '.
			'CDEF:c=cc,1024,* '.
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'LINE1:a#000000:"Total " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:b#FF0000:"Used  " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:c#00CF00:"Free  " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:b#404040 ';
		break;

	case 'mysql':
		switch ($tmp1[1]) {
		case 'commands':
			$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"commands\" ".
				"DEF:aa={$rrdfile}:com_delete:AVERAGE ".
				"DEF:bb={$rrdfile}:com_insert:AVERAGE ".
				"DEF:cc={$rrdfile}:com_select:AVERAGE ".
				"DEF:dd={$rrdfile}:com_update:AVERAGE ".
				"DEF:ee={$rrdfile}:questions:AVERAGE ".
				'CDEF:a=aa,60,* '.
				'CDEF:b=bb,60,* '.
				'CDEF:c=cc,60,* '.
				'CDEF:d=dd,60,* '.
				'CDEF:e=ee,60,* '.
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'AREA:e#0000FF:"All Commands " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
				'AREA:a#FF0000:"DELETE ...   " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:b#FF7D00:"INSERT ...   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:d#FFC73B:"UPDATE ...   " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
				'STACK:c#00CF00:"SELECT ...   " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
				'';
			break;

		case 'qcache':
			$cmd .= "--base=1024 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"???\" ".
				"DEF:a={$rrdfile}:qc_free_blocks:AVERAGE ".
				"DEF:b={$rrdfile}:qc_free_memory:AVERAGE ".
				"DEF:cc={$rrdfile}:qc_hits:AVERAGE ".
				"DEF:dd={$rrdfile}:qc_inserts:AVERAGE ".
				"DEF:ee={$rrdfile}:qc_lowmem_prunes:AVERAGE ".
				"DEF:ff={$rrdfile}:qc_not_cached:AVERAGE ".
				"DEF:g={$rrdfile}:qc_queries_in_cache:AVERAGE ".
				"DEF:h={$rrdfile}:qc_total_blocks:AVERAGE ".
				'CDEF:c=cc,60,* '.
				'CDEF:d=dd,60,* '.
				'CDEF:e=ee,60,* '.
				'CDEF:f=ff,60,* '.
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'LINE1:a#FF0000:"qc_free_blocks     " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:b#FFFF00:"qc_free_memory     " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:c#00FF00:"qc_hits            " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:d#00FFFF:"qc_inserts         " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:e#0000FF:"qc_lowmemprunes    " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:f#FF00FF:"qc_not_cached      " GPRINT:f:LAST:"Current\:%8.2lf %s" GPRINT:f:AVERAGE:"Average\:%8.2lf %s" GPRINT:f:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:g#FF00FF:"qc_queries_in_cache" GPRINT:g:LAST:"Current\:%8.2lf %s" GPRINT:g:AVERAGE:"Average\:%8.2lf %s" GPRINT:g:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:h#000000:"qc_total_blocks    " GPRINT:h:LAST:"Current\:%8.2lf %s" GPRINT:h:AVERAGE:"Average\:%8.2lf %s" GPRINT:h:MAX:"Maximum\:%8.2lf %s\n" '.
				'';
			break;

		case 'threads':
			$cmd .= "--base=1024 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"threads\" ".
				"DEF:a={$rrdfile}:threads_cached:AVERAGE ".
				"DEF:b={$rrdfile}:threads_connected:AVERAGE ".
				"DEF:cc={$rrdfile}:threads_created:AVERAGE ".
				"DEF:d={$rrdfile}:threads_running:AVERAGE ".
				'CDEF:c=cc,60,* '.
				'CDEF:err=a,UN,INF,UNKN,IF '.
				'AREA:err#FFD0D0 '.
				'LINE1:c#00CF00:"threads created  " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:d#FF0000:"threads running  " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:b#00FFFF:"threads connected" GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
				'LINE1:a#0000FF:"threads cached   " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
				'';
			break;

		case 'traffic':
			$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"bytes per second\" ".
				"DEF:imax={$rrdfile}:bytes_received:MAX ".
				"DEF:iavg={$rrdfile}:bytes_received:AVERAGE ".
				"DEF:imin={$rrdfile}:bytes_received:MIN ".
				"DEF:omax={$rrdfile}:bytes_sent:MAX ".
				"DEF:oavg={$rrdfile}:bytes_sent:AVERAGE ".
				"DEF:omin={$rrdfile}:bytes_sent:MIN ".
				'CDEF:omaxn=omax,-1,* CDEF:oavgn=oavg,-1,* CDEF:ominn=omin,-1,* '.
				'CDEF:imid=imax,imin,- CDEF:omid=omaxn,ominn,- '.
				'CDEF:oerr=oavg,UN,INF,UNKN,IF CDEF:ierr=iavg,UN,-INF,UNKN,IF '.
				'HRULE:0#FF0000 AREA:ierr#FFD0D0 AREA:oerr#FFD0D0 '.
				'AREA:imin STACK:imid#A0FFA0:"min/max Inbound " GPRINT:imin:MIN:"%6.2lf%s" GPRINT:imax:MAX:"%6.2lf%s" '.
				'LINE1:imin#a0a0a0 LINE1:imax#a0a0a0 LINE1:iavg#008000:"avg Inbound " '.
				'GPRINT:iavg:MIN:"min\: %6.2lf%s" GPRINT:iavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:iavg:MAX:"max\: %6.2lf%s\n" '.
				'AREA:ominn STACK:omid#C0C0FF:"min/max Outbound" GPRINT:omin:MIN:"%6.2lf%s" GPRINT:omax:MAX:"%6.2lf%s" '.
				'LINE1:ominn#a0a0a0 LINE1:omaxn#a0a0a0 LINE1:oavgn#000080:"avg Outbound" '.
				'GPRINT:oavg:MIN:"min\: %6.2lf%s" GPRINT:oavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:oavg:MAX:"max\: %6.2lf%s\n" ';
			break;

		}
		break;

	case 'net':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"bytes per second\" ".
			"DEF:imax={$rrdfile}:byte_in:MAX ".
			"DEF:iavg={$rrdfile}:byte_in:AVERAGE ".
			"DEF:imin={$rrdfile}:byte_in:MIN ".
			"DEF:omax={$rrdfile}:byte_out:MAX ".
			"DEF:oavg={$rrdfile}:byte_out:AVERAGE ".
			"DEF:omin={$rrdfile}:byte_out:MIN ".
			'CDEF:omaxn=omax,-1,* CDEF:oavgn=oavg,-1,* CDEF:ominn=omin,-1,* '.
			'CDEF:imid=imax,imin,- CDEF:omid=omaxn,ominn,- '.
			'CDEF:oerr=oavg,UN,INF,UNKN,IF CDEF:ierr=iavg,UN,-INF,UNKN,IF '.
			'HRULE:0#FF0000 AREA:ierr#FFD0D0 AREA:oerr#FFD0D0 '.
			'AREA:imin#E0FFE0 STACK:imid#A0FFA0:"min/max Inbound " GPRINT:imin:MIN:"%6.2lf%s" GPRINT:imax:MAX:"%6.2lf%s" '.
			'LINE1:imin#a0a0a0 LINE1:imax#a0a0a0 LINE1:iavg#008000:"avg Inbound " '.
			'GPRINT:iavg:MIN:"min\: %6.2lf%s" GPRINT:iavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:iavg:MAX:"max\: %6.2lf%s\n" '.
			'AREA:ominn#E0E0FF STACK:omid#C0C0FF:"min/max Outbound" GPRINT:omin:MIN:"%6.2lf%s" GPRINT:omax:MAX:"%6.2lf%s" '.
			'LINE1:ominn#a0a0a0 LINE1:omaxn#a0a0a0 LINE1:oavgn#000080:"avg Outbound" '.
			'GPRINT:oavg:MIN:"min\: %6.2lf%s" GPRINT:oavg:AVERAGE:"avg\: %6.2lf%s" GPRINT:oavg:MAX:"max\: %6.2lf%s\n" ';
		break;

	case 'proc':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"per second\" ".
			"DEF:a={$rrdfile}:intr:AVERAGE ".
			"DEF:b={$rrdfile}:ctxt:AVERAGE ".
			"DEF:c={$rrdfile}:fork:AVERAGE ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:b#00AF00:"Context   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#FF0000:"Interrupts" GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:c#0000FF:"Forks     " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" ';
		break;

	case 'random':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit 0 --vertical-label=\"bytes\" ".
			"DEF:a={$rrdfile}:entropy:AVERAGE ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:a#00CF00:"Available Entropy" GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:a#404040 ';
		break;

	case 'rtcache':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"calls\" ".
			"DEF:a={$rrdfile}:entries:AVERAGE ".
			"DEF:b={$rrdfile}:gc_total:AVERAGE ".
			"DEF:c={$rrdfile}:gc_ignored:AVERAGE ".
			"DEF:d={$rrdfile}:gc_goal_miss:AVERAGE ".
			"DEF:e={$rrdfile}:gc_dst_overflow:AVERAGE ".
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'LINE1:a#FF0000:"entries         " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:b#FF00FF:"gc_total        " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:c#0000FF:"gc_ignored      " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:d#007FFF:"gc_goal_miss    " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:e#00FFFF:"gc_dst_overflow " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" ';
		break;

	case 'rtstat':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"calls\" ".
			"DEF:a={$rrdfile}:in_hit:AVERAGE ".
			"DEF:b={$rrdfile}:in_slow_tot:AVERAGE ".
			"DEF:c={$rrdfile}:in_slow_mc:AVERAGE ".
			"DEF:d={$rrdfile}:in_no_route:AVERAGE ".
			"DEF:e={$rrdfile}:in_brd:AVERAGE ".
			"DEF:f={$rrdfile}:in_martian_dst:AVERAGE ".
			"DEF:g={$rrdfile}:in_martian_src:AVERAGE ".
			"DEF:h={$rrdfile}:in_hlist_search:AVERAGE ".
			"DEF:i={$rrdfile}:out_hit:AVERAGE ".
			"DEF:j={$rrdfile}:out_slow_tot:AVERAGE ".
			"DEF:k={$rrdfile}:out_slow_mc:AVERAGE ".
			"DEF:l={$rrdfile}:out_hlist_search:AVERAGE ".
			'CDEF:ii=i,-1,* '.
			'CDEF:jj=j,-1,* '.
			'CDEF:kk=k,-1,* '.
			'CDEF:ll=l,-1,* '.
			'CDEF:oerr=a,UN,INF,UNKN,IF CDEF:ierr=i,UN,-INF,UNKN,IF '.
			'AREA:ierr#FFD0D0 AREA:oerr#FFD0D0 '.
			'LINE1:a#FF0000:"in_hit           " GPRINT:a:LAST:"Current\:%8.0lf" GPRINT:a:AVERAGE:"Average\:%8.0lf" GPRINT:a:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:b#FF00FF:"in_slow_tot      " GPRINT:b:LAST:"Current\:%8.0lf" GPRINT:b:AVERAGE:"Average\:%8.0lf" GPRINT:b:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:c#0000FF:"in_slow_mc       " GPRINT:c:LAST:"Current\:%8.0lf" GPRINT:c:AVERAGE:"Average\:%8.0lf" GPRINT:c:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:d#007FFF:"in_no_route      " GPRINT:d:LAST:"Current\:%8.0lf" GPRINT:d:AVERAGE:"Average\:%8.0lf" GPRINT:d:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:e#00FFFF:"in_brd           " GPRINT:e:LAST:"Current\:%8.0lf" GPRINT:e:AVERAGE:"Average\:%8.0lf" GPRINT:e:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:f#00FF00:"in_martian_dst   " GPRINT:f:LAST:"Current\:%8.0lf" GPRINT:f:AVERAGE:"Average\:%8.0lf" GPRINT:f:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:g#FFFF00:"in_martian_src   " GPRINT:g:LAST:"Current\:%8.0lf" GPRINT:g:AVERAGE:"Average\:%8.0lf" GPRINT:g:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:h#FF7F00:"in_hlist_search  " GPRINT:h:LAST:"Current\:%8.0lf" GPRINT:h:AVERAGE:"Average\:%8.0lf" GPRINT:h:MAX:"Maximum\:%8.0lf\n" '.
			'HRULE:0#FF0000:"-\n" '.
			'LINE1:ii#FF0000:"out_hit          " GPRINT:i:LAST:"Current\:%8.0lf" GPRINT:i:AVERAGE:"Average\:%8.0lf" GPRINT:i:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:jj#FF00FF:"out_slow_tot     " GPRINT:j:LAST:"Current\:%8.0lf" GPRINT:j:AVERAGE:"Average\:%8.0lf" GPRINT:j:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:kk#0000FF:"out_slow_mc      " GPRINT:k:LAST:"Current\:%8.0lf" GPRINT:k:AVERAGE:"Average\:%8.0lf" GPRINT:k:MAX:"Maximum\:%8.0lf\n" '.
			'LINE1:ll#FF7F00:"out_hlist_search " GPRINT:l:LAST:"Current\:%8.0lf" GPRINT:l:AVERAGE:"Average\:%8.0lf" GPRINT:l:MAX:"Maximum\:%8.0lf\n" ';
		break;

	case 'swap':
		$cmd .= "--base=1024 --height={$height} --width={$width} --alt-autoscale-max --lower-limit=0 --vertical-label=\"kB\" ".
			"DEF:aa={$rrdfile}:total:AVERAGE ".
			"DEF:bb={$rrdfile}:free:AVERAGE ".
			'CDEF:a=aa,1024,* '.
			'CDEF:b=bb,1024,* '.
			'CDEF:x=aa,bb,-,1024,* '.
			'CDEF:err=a,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'LINE1:a#000000:"Total Swap  " GPRINT:a:LAST:"Current\:%8.2lf %s\n" '.
			'AREA:x#FF0000:"Used Swap   " GPRINT:x:LAST:"Current\:%8.2lf %s" GPRINT:x:AVERAGE:"Average\:%8.2lf %s" GPRINT:x:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"Free Swap   " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" ';
		break;

	case 'ts2':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit 0 --vertical-label=\"users/chans\" ".
			"DEF:users={$rrdfile}:users:AVERAGE ".
			"DEF:chans={$rrdfile}:channels:AVERAGE ".
			'CDEF:err=users,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:chans#00CF00:"Channels \n" '.
			'LINE1:users#002A97:"Users "';
		break;

	case 'uptime':
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --lower-limit 0 --vertical-label=\"days\" ".
			"DEF:ups={$rrdfile}:uptime:AVERAGE ".
			"DEF:idles={$rrdfile}:idletime:AVERAGE ".
			'CDEF:up=ups,86400,/ '.
			'CDEF:idle=idles,86400,/ '.
			'CDEF:err=ups,UN,INF,UNKN,IF '.
			'AREA:err#FFD0D0 '.
			'AREA:up#00CF00:"Uptime \n" '.
			'LINE1:idle#002A97:"Idletime "';
		break;

	case 'vmstat':
		$height *= 2;
		$cmd .= "--base=1000 --height={$height} --width={$width} --alt-autoscale-max --vertical-label=\"allocs\" ".
			"DEF:a={$rrdfile}:pgalloc_high:AVERAGE ".
			"DEF:b={$rrdfile}:pgalloc_normal:AVERAGE ".
			"DEF:c={$rrdfile}:pgalloc_dma:AVERAGE ".
			"DEF:d={$rrdfile}:pgfree:AVERAGE ".
			"DEF:e={$rrdfile}:pgfault:AVERAGE ".
			"CDEF:ee=e,-1,* ".
			'CDEF:oerr=a,UN,INF,UNKN,IF CDEF:ierr=e,UN,-INF,UNKN,IF '.
			'AREA:ierr#FFD0D0 AREA:oerr#FFD0D0 '.
			'HRULE:0#FF0000: '.
			'AREA:c#FF0000:"pgalloc_dma    " GPRINT:c:LAST:"Current\:%8.2lf %s" GPRINT:c:AVERAGE:"Average\:%8.2lf %s" GPRINT:c:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:b#00CF00:"pgalloc_normal " GPRINT:b:LAST:"Current\:%8.2lf %s" GPRINT:b:AVERAGE:"Average\:%8.2lf %s" GPRINT:b:MAX:"Maximum\:%8.2lf %s\n" '.
			'STACK:a#00CFCF:"pgalloc_high   " GPRINT:a:LAST:"Current\:%8.2lf %s" GPRINT:a:AVERAGE:"Average\:%8.2lf %s" GPRINT:a:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:d#FF7D00:"pgfree         " GPRINT:d:LAST:"Current\:%8.2lf %s" GPRINT:d:AVERAGE:"Average\:%8.2lf %s" GPRINT:d:MAX:"Maximum\:%8.2lf %s\n" '.
			'AREA:ee#0040FF:"pgfault        " GPRINT:e:LAST:"Current\:%8.2lf %s" GPRINT:e:AVERAGE:"Average\:%8.2lf %s" GPRINT:e:MAX:"Maximum\:%8.2lf %s\n" '.
			'LINE1:ee#002A97 ';
		break;
	}

	header('Content-type: image/x-png');
	passthru($cmd);
	die();
}

$action = $_GET['action'];
switch ($action) {
case 'init':
	$conf = create_config();
	write_config(CONFIG, $conf);
	header("Location: {$_SERVER['SCRIPT_NAME']}?action=setup");
	break;


case 'setup':
	$conf = read_config(CONFIG);
	$conf = setup($conf);
	if (isset($conf)) {
		write_config(CONFIG, $conf);
		header("Location: {$_SERVER['SCRIPT_NAME']}?action=setup");
	}
	break;

case 'rrd':
	$conf = read_config(CONFIG);
	show_rrd($conf);
	break;

default:
case 'show':
	$conf = read_config(CONFIG);
	show_html($conf);
	break;
}

?>
