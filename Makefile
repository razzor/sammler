# Toplevel Makefile
WITH_RRD?=yes

PLUGINS := ctstat diskstat hwmon load memory mount netdev random rtstat stat uptime vmstat
#PLUGINS += apache mysql conntrack alixusv ts2 diskstandby hddtemp

DESTDIR =
BINARY_DIR = /usr/local/bin
CONFIG_DIR = /usr/local/etc
PLUGIN_DIR = /usr/local/lib/sammler
DATA_DIR = /var/lib/sammler
LOG_DIR = /var/log
PID_DIR = /var/run
WWW_DIR = /var/www
WWW_CONFIG = $(WWW_DIR)/sammler_graph.conf
WWW_OWNER = www-data

# ############################

SRC := $(wildcard *.c)
BUILD_DIR = build
CFLAGS = -O2 -g -pipe -Wall -Wno-unused-result -I.
CFLAGS += -MMD -MF $(BUILD_DIR)/$(*D)/$(*F).d
LDFLAGS = -rdynamic -ldl

# ############################

ifeq ("$(WITH_RRD)", "yes")
	CFLAGS += -DWITH_RRD
	LDFLAGS_TARGET = -lrrd
	TARGET = sammler
else
	LDFLAGS_TARGET =
	TARGET = sammler_norrd
endif

ifeq ($(strip $(wildcard $(DESTDIR)$(CONFIG_DIR)/sammler.conf)),)
	NEWCONF=sammler.conf
else
	NEWCONF=sammler.conf.dist
endif

# ############################

all: $(TARGET) plugins

$(TARGET): $(patsubst %,$(BUILD_DIR)/%, $(SRC:.c=.o))
	@echo " Linking file:  $@"
	@$(CC) $^ $(LDFLAGS) $(LDFLAGS_TARGET) -o $@

$(BUILD_DIR)/%.o: %.c $(MAKEFILE_LIST)
	@echo " Building file: $<"
	@$(shell test -d $(BUILD_DIR)/$(*D) || mkdir -p $(BUILD_DIR)/$(*D))
	@$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: plugins
plugins: $(PLUGINS:%=plugins/%.so)

.SECONDARY: $(patsubst %,$(BUILD_DIR)/%, $(PLUGINS:%=plugins/%.o))
plugins/%.so: $(BUILD_DIR)/plugins/%.o
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) -shared -o $@ $<

plugins/apache.so: $(BUILD_DIR)/plugins/apache.o
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) -shared -lcurl -o $@ $<

plugins/conntrack.so: $(BUILD_DIR)/plugins/conntrack.o
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) -shared -lnfnetlink -lnetfilter_conntrack -o $@ $<

plugins/mysql.so: $(BUILD_DIR)/plugins/mysql.o $(BUILD_DIR)/plugins/mysql_helper.o
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) -shared -lmysqlclient -o $@ $^

plugins/diskstandby.so: $(BUILD_DIR)/plugins/diskstandby.o $(BUILD_DIR)/plugins/sgio.o
	@echo " Linking file:  $@"
	@$(CC) $(LDFLAGS) -shared -o $@ $^

install: all
	install -D -m 755 -s $(TARGET) $(DESTDIR)$(BINARY_DIR)/$(TARGET)
	install -D -m 644 sammler.conf $(DESTDIR)$(CONFIG_DIR)/$(NEWCONF)
	sed -i -e "s:^logfile .*$$:logfile $(LOG_DIR)/sammler.log:" \
		-e "s:^pidfile .*$$:pidfile $(PID_DIR)/sammler.pid:" \
		-e "s:^rrd_dir .*$$:rrd_dir $(DATA_DIR):" \
		-e "s:^plugin_dir .*$$:plugin_dir $(PLUGIN_DIR):" \
		$(DESTDIR)$(CONFIG_DIR)/$(NEWCONF)
	install -d -m 755 $(DESTDIR)$(PLUGIN_DIR)
	install -m 755 -s plugins/*.so $(DESTDIR)$(PLUGIN_DIR)
	install -d -m 755 $(DESTDIR)$(LOG_DIR)


ifeq ("$(WITH_RRD)", "yes")
	install -d -m 755 $(DESTDIR)$(DATA_DIR)
	install -D -m 644 sammler.php $(DESTDIR)$(WWW_DIR)/sammler.php
	sed -i -e "s:%%WWW_CONFIG%%:$(WWW_CONFIG):" \
		-e "s:%%DATA_DIR%%:$(DATA_DIR)/:" \
		$(DESTDIR)$(WWW_DIR)/sammler.php
ifeq ($(strip $(wildcard $(DESTDIR)$(WWW_CONFIG))),)
	-install -D -m 644 -o $(WWW_OWNER) /dev/null $(DESTDIR)$(WWW_CONFIG)
endif
else
	sed -i -e "s:^forward_only .*$$:forward_only true:" \
		$(DESTDIR)$(CONFIG_DIR)/$(NEWCONF)
endif

clean:
	rm -rf build plugins/*.so $(TARGET)

include $(shell find $(BUILD_DIR) -name \*.d 2> /dev/null)
