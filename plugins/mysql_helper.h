#ifndef _MYSQL_HELPER_H_
#define _MYSQL_HELPER_H_

#include <stdint.h>

struct mysql_stats {
	uint64_t bytes_received;
	uint64_t bytes_sent;
	uint64_t com_delete;
	uint64_t com_insert;
	uint64_t com_select;
	uint64_t com_update;
	uint64_t connections;
	uint64_t qc_free_blocks;
	uint64_t qc_free_memory;
	uint64_t qc_hits;
	uint64_t qc_inserts;
	uint64_t qc_lowmem_prunes;
	uint64_t qc_not_cached;
	uint64_t qc_queries_in_cache;
	uint64_t qc_total_blocks;
	uint64_t questions;
	uint64_t threads_cached;
	uint64_t threads_connected;
	uint64_t threads_created;
	uint64_t threads_running;
};

void * init_connection(const char *host, const char *user, const char *pass);
int ping_connection(void *mysql);
int close_connection(void *mysql);

int get_stats(void *mysql, struct mysql_stats *stats);

#endif /* _MYSQL_HELPER_H_ */
