/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

struct sammler_plugin plugin;

static const char *ds_def = {
	"DS:uptime:GAUGE:15:0:U "
	"DS:idletime:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	FILE *fp = fopen("/proc/uptime", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin uptime: fopen()");
		return -1;
	}

	double uptime, idletime;
	if (fscanf(fp, "%lf %lf\n", &uptime, &idletime) != 2) {
		log_print(LOG_WARN, "plugin uptime: fscanf()");
		fclose(fp);
		return -1;
	}
	fclose(fp);

	probe_submit(&plugin, "uptime.rrd", 0, "%.02lf:%.02lf", uptime, idletime);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "uptime",
	.interval	= 10,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
