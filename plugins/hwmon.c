/***************************************************************************
 *   Copyright (C) 10/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "configfile.h"
#include "helper.h"
#include "list.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

struct sammler_plugin plugin;

struct hwmon_entry {
	struct list_head list;
	char *name;
	char *path;
};

static LIST_HEAD(hwmon_list);

static const char *ds_def = {
	"DS:temp:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	struct hwmon_entry *entry;
	list_for_each_entry(entry, &hwmon_list, list) {
		FILE *fp = fopen(entry->path, "r");
		if (fp == NULL) {
			log_print(LOG_WARN, "plugin hwmon: fopen(%s)", entry->path);
			continue;
		}

		int temp;
		if (fscanf(fp, "%d\n", &temp) != 1) {
			log_print(LOG_WARN, "plugin hwmon: fscanf()");
			fclose(fp);
			continue;
		}
		fclose(fp);

		char filename[32];
		int len = snprintf(filename, sizeof(filename), "hwmon-%s.rrd", entry->name);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, 0, "%d", temp);
	}
	return 0;
}

static int init_cb(struct strtoken *tokens, void *privdata)
{
	if (tokens->count != 2) {
		log_print(LOG_ERROR, "p_hwmon: parse error");
		return -1;
	}

	struct hwmon_entry *entry = malloc(sizeof(struct hwmon_entry));
	if (entry == NULL) {
		log_print(LOG_ERROR, "p_hwmon: out of memory");
		return -1;
	}

	entry->name = strdup(tokens->field[0]);
	entry->path = strdup(tokens->field[1]);

	log_print(LOG_DEBUG, "p_hwmon: added sensor '%s' (%s)", entry->name, entry->path);
	list_add_tail(&entry->list, &hwmon_list);

	return 0;
}

static int init(void)
{
	config_get_strtokens("p_hwmon", "temp", ",", 2, init_cb, NULL);
	return 0;
}

static int fini(void)
{
	struct hwmon_entry *entry, *tmp;
	list_for_each_entry_safe(entry, tmp, &hwmon_list, list) {
		free(entry->name);
		free(entry->path);
		free(entry);
	}

	return 0;
}

struct sammler_plugin plugin = {
	.name		= "hwmon",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
