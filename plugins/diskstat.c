/***************************************************************************
 *   Copyright (C) 04/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *ds_def = {
	"DS:read_cnt:COUNTER:15:0:U "
	"DS:read_sec:COUNTER:15:0:U "
	"DS:read_ms:COUNTER:15:0:U "
	"DS:write_cnt:COUNTER:15:0:U "
	"DS:write_sec:COUNTER:15:0:U "
	"DS:write_ms:COUNTER:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	FILE *fp = fopen("/proc/diskstats", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin diskstats: fopen()");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		int major, minor;
		char device[16];
		unsigned int val[11];
		int cnt = sscanf(buffer, "%d %d %s %u %u %u %u %u %u %u %u %u %u %u",
					&major, &minor, device, &val[0],
					&val[1], &val[2], &val[3], &val[4],
					&val[5], &val[6], &val[7], &val[8],
					&val[9], &val[10]);

		if (cnt != 14 || val[0] == 0 || val[1] == 0)
			continue;

		char filename[32];
		int len = snprintf(filename, sizeof(filename), "disk-%s.rrd", device);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, 0,
				"%u:%u:%u:%u:%u:%u",
				val[0], val[2], val[3],
				val[4], val[6], val[7]);
	}

	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "diskstat",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
