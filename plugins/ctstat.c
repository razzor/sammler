/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helper.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *ds_def = {
	"DS:entries:GAUGE:15:0:U "
	"DS:searched:DERIVE:15:0:U "
	"DS:found:DERIVE:15:0:U "
	"DS:new:DERIVE:15:0:U "
	"DS:invalid:DERIVE:15:0:U "
	"DS:ignore:DERIVE:15:0:U "
	"DS:delete:DERIVE:15:0:U "
	"DS:delete_list:DERIVE:15:0:U "
	"DS:insert:DERIVE:15:0:U "
	"DS:insert_failed:DERIVE:15:0:U "
	"DS:drop:DERIVE:15:0:U "
	"DS:early_drop:DERIVE:15:0:U "
	"DS:icmp_error:DERIVE:15:0:U "
	"DS:expect_new:DERIVE:15:0:U "
	"DS:expect_create:DERIVE:15:0:U "
	"DS:expect_delete:DERIVE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	FILE *fp;
	char *val[16], filename[16];
	unsigned long long arr[16];
	int i, len, cpu = 0;

	fp = fopen("/proc/net/stat/ip_conntrack", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin ctstat");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		if (!strncmp(buffer, "entries", 7))
			continue;

		if (strsplit(buffer, " \t\n", val, 16) != 16)
			continue;

		for (i = 0; i < 16; i++)
			arr[i] = strtoll(val[i], NULL, 16);

		len = snprintf(filename, sizeof(filename), "ctstat-%d.rrd", cpu);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, 0,
				"%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu",
				arr[0], arr[1], arr[2], arr[3],
				arr[4], arr[5], arr[6], arr[7],
				arr[8], arr[9], arr[10], arr[11],
				arr[12], arr[13], arr[14], arr[15]);

		cpu++;
	}
	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "ctstat",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
