/***************************************************************************
 *   Copyright (C) 04/2009 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>

#include "configfile.h"
#include "helper.h"
#include "list.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

struct sammler_plugin plugin;

static const char *socket_path;

static const char *ds_def = {
	"DS:ibat:GAUGE:15:U:U "
	"DS:ubat:GAUGE:15:0:U "
	"DS:uin:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (sockfd == -1) {
		log_print(LOG_WARN, "plugin alixusv: socket()");
		return -1;
	}

	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path));
	int len = sizeof(addr.sun_family) + strlen(addr.sun_path);

	if (connect(sockfd, (struct sockaddr *)&addr, len) < 0) {
		log_print(LOG_ERROR, "plugin alixusv: connect(%s)", addr.sun_path);
		close(sockfd);
		return 0;
	}

	char buf[64] = "status";
	write(sockfd, buf, strlen(buf));
	
	len = read(sockfd, buf, sizeof(buf));
	if (len <= 0) {
		log_print(LOG_ERROR, "plugin alixusv: read()");
		close(sockfd);
		return -1;
	}

	close(sockfd);

	char *field[4];
	if (strsplit(buf, ": \t\n", field, 4) != 4)
		return -1;

	int ibat = strtoll(field[1], NULL, 10);
	int ubat = strtoll(field[2], NULL, 10);
	int uin = strtoll(field[3], NULL, 10);

	probe_submit(&plugin, "alixusv.rrd", 0, "%d:%d:%d", ibat, ubat, uin);
	return 0;
}

static int init(void)
{
	socket_path = config_get_string("p_alixusv", "socket", NULL);
	return (socket_path == NULL) ? -1 : 0;
}

struct sammler_plugin plugin = {
	.name		= "alixusv",
	.interval	= 10,
	.init		= &init,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
