#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mysql/mysql.h>

#include "logging.h"
#include "mysql_helper.h"

void * init_connection(const char *host, const char *user, const char *pass)
{
	MYSQL *con = NULL;

	con = mysql_init(con);
	if (con == NULL)
		return NULL;

	con = mysql_real_connect(con, host, user, pass, NULL, 0, NULL, 0);
	if (con == NULL) {
		log_print(LOG_ERROR, "p_mysql: %s", mysql_error(con));
		return NULL;
	}

	return con;
}

int ping_connection(void *mysql)
{
	MYSQL *con = mysql;

	return mysql_ping(con);
}

int close_connection(void *mysql)
{
	MYSQL *con = mysql;

	mysql_close(con);
	return 0;
}

int get_stats(void *mysql, struct mysql_stats *stats)
{
	MYSQL *con = mysql;

	char *query;
	if (mysql_get_server_version(con) >= 50002)
		query = "SHOW GLOBAL STATUS";
	else
		query = "SHOW STATUS";

	if (mysql_real_query(con, query, strlen(query))) {
		log_print(LOG_ERROR, "p_mysql: %s", mysql_error(con));
		return -1;
	}

	MYSQL_RES *res;
	res = mysql_store_result(con);
	if (res == NULL) {
		log_print(LOG_ERROR, "p_mysql: %s", mysql_error(con));
		return -1;
	}

	MYSQL_ROW row;
	while ((row = mysql_fetch_row(res))) {
		char *key;
		unsigned long long val;

		key = row[0];
		val = atoll(row[1]);

		if (!strncmp(key, "Bytes_", 6)) {
			if (!strcmp(key +6, "received"))
				stats->bytes_received = val;

			else if (!strcmp(key +6, "sent"))
				stats->bytes_sent = val;

		} else if (!strncmp(key, "Com_", 4)) {
			if (!strcmp(key +4, "delete"))
				stats->com_delete = val;

			else if (!strcmp(key +4, "insert"))
				stats->com_insert = val;

			else if (!strcmp(key +4, "select"))
				stats->com_select = val;

			else if (!strcmp(key +4, "update"))
				stats->com_update = val;

		} else if (!strcmp(key, "Connections")) {
			stats->connections = val;

		} else if (!strncmp(key, "Qcache_", 7)) {
			if (!strcmp(key +7, "free_blocks"))
				stats->qc_free_blocks = val;

			else if (!strcmp(key +7, "free_memory"))
				stats->qc_free_memory = val;

			else if (!strcmp(key +7, "hits"))
				stats->qc_hits = val;

			else if (!strcmp(key +7, "inserts"))
				stats->qc_inserts = val;

			else if (!strcmp(key +7, "lowmem_prunes"))
				stats->qc_lowmem_prunes = val;

			else if (!strcmp(key +7, "not_cached"))
				stats->qc_not_cached = val;

			else if (!strcmp(key +7, "queries_in_cache"))
				stats->qc_queries_in_cache = val;

			else if (!strcmp(key +7, "total_blocks"))
				stats->qc_total_blocks = val;

		} else if (!strcmp(key, "Questions")) {
			stats->questions = val;

		} else if (!strncmp(key, "Threads_", 8)) {
			if (!strcmp(key, "Threads_cached"))
				stats->threads_cached = val;

			else if (!strcmp(key, "Threads_connected"))
				stats->threads_connected = val;

			else if (!strcmp(key, "Threads_created"))
				stats->threads_created = val;

			else if (!strcmp(key, "Threads_running"))
				stats->threads_running = val;
		}
	}

	mysql_free_result(res);

	return 0;
}
