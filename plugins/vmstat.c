/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *ds_def = {
	"DS:pgalloc_high:DERIVE:15:0:U "
	"DS:pgalloc_normal:DERIVE:15:0:U "
	"DS:pgalloc_dma:DERIVE:15:0:U "
	"DS:pgfree:DERIVE:15:0:U "
	"DS:pgfault:DERIVE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

struct vmstat_ {
	unsigned long long pgalloc_high;
	unsigned long long pgalloc_normal;
	unsigned long long pgalloc_dma;
	unsigned long long pgfree;
	unsigned long long pgfault;
};

static int probe(void)
{
	FILE *fp;
	struct vmstat_ vmstat;

	memset(&vmstat, 0, sizeof(vmstat));

	fp = fopen("/proc/vmstat", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin vmstat");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		if (!strncmp(buffer, "pgalloc_high", 12))
			vmstat.pgalloc_high = atoll(buffer + 13);

		else if (!strncmp(buffer, "pgalloc_normal", 14))
			vmstat.pgalloc_normal = atoll(buffer + 15);

		else if (!strncmp(buffer, "pgalloc_dma", 11))
			vmstat.pgalloc_dma = atoll(buffer + 12);

		else if (!strncmp(buffer, "pgfree", 6))
			vmstat.pgfree = atoll(buffer + 7);

		else if (!strncmp(buffer, "pgfault", 7))
			vmstat.pgfault = atoll(buffer + 8);
	}

	probe_submit(&plugin, "vmstat.rrd", 0, "%llu:%llu:%llu:%llu:%llu",
			vmstat.pgalloc_high, vmstat.pgalloc_normal,
			vmstat.pgalloc_dma, vmstat.pgfree, vmstat.pgfault);

	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "vmstat",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
