/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mntent.h>
#include <sys/vfs.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define MAXFSNAME 16

struct sammler_plugin plugin;

static const char *ds_def = {
	"DS:block_total:GAUGE:15:0:U "
	"DS:block_free:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static char * get_valid_fs(int *xcnt)
{
	FILE *fp = fopen("/proc/filesystems", "r");
	if (fp == NULL)
		return NULL;

	int cnt = 0;
	char buffer[64];
	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		if (!strncmp(buffer, "nodev", 5))
			continue;

		cnt++;
	}

	char *valid_arr = malloc(cnt * MAXFSNAME);
	if (valid_arr == NULL)
		return NULL;

	rewind(fp);

	int i = 0;
	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		if (!strncmp(buffer, "nodev", 5))
			continue;

		char *end = memccpy(valid_arr + (i++ * MAXFSNAME), buffer +1, '\n', MAXFSNAME);
		*(end -1) = '\0';
	}

	fclose(fp);
	*xcnt = cnt;
	return valid_arr;
}

static int probe(void)
{
	FILE *fp = setmntent("/etc/mtab", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin mount");
		return -1;
	}

	int cnt;
	char *valid_arr = get_valid_fs(&cnt);
	if (valid_arr == NULL) {
		log_print(LOG_WARN, "plugin mount");
		endmntent(fp);
		return -1;
	}

	struct mntent *mnt;
	while ((mnt = getmntent(fp)) != NULL) {

		int i, valid = 0;
		for (i = 0; i < cnt; i++)
			if (strncmp(mnt->mnt_type, valid_arr + (i * MAXFSNAME), MAXFSNAME) == 0)
				valid = 1;

		if (valid == 0)
			continue;

		struct statfs fs;
		if (statfs(mnt->mnt_dir, &fs) == -1) {
			log_print(LOG_WARN, "plugin mount: statfs(%s)", mnt->mnt_dir);
			continue;
		}

		if (fs.f_blocks == 0)
			continue;

		char *slash = mnt->mnt_fsname;
		while (slash && (slash = strchr(slash, '/'))) {
			slash = strchr(slash, '/');
			*slash++ = '_';
		}

		char filename[64];
		int len = snprintf(filename, sizeof(filename), "mount%s.rrd", mnt->mnt_fsname);
		if (len < 0 || len >= sizeof(filename)) {
			log_print(LOG_WARN, "plugin mount: file name too long", mnt->mnt_fsname);
			continue;
		}

		probe_submit(&plugin, filename, 0, "%lu:%lu",
				fs.f_blocks * (fs.f_bsize /1024),
				fs.f_bfree * (fs.f_bsize /1024));
	}

	free(valid_arr);
	endmntent(fp);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "mount",
	.interval	= 10,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
