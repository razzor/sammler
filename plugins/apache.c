/***************************************************************************
 *   Copyright (C) 10/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <curl/curl.h>

#include "configfile.h"
#include "helper.h"
#include "linebuffer.h"
#include "list.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define BUFSIZE	8192

struct sammler_plugin plugin;

struct server_entry {
	struct list_head list;
	CURL *handle;
	char *name;
};

static LIST_HEAD(server_list);

static const char *ds_def = {
	"DS:total_accesses:COUNTER:90:0:U "
	"DS:total_kbytes:COUNTER:90:0:U "
	"DS:busy_workers:GAUGE:90:0:U "
	"DS:idle_workers:GAUGE:90:0:U "
};

struct stats {
	uint64_t	total_accesses;
	uint64_t	total_kbytes;
	uint64_t	busy_workers;
	uint64_t	idle_workers;
};

static struct lbuf * rxbuf;

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static size_t curl_callback(void *buffer, size_t size, size_t nmemb, void *userp)
{
	size_t realsize = size * nmemb;

	lbuf_append(rxbuf, buffer, realsize);

	return realsize;
}

static int probe(void)
{
	struct server_entry *entry;
	list_for_each_entry(entry, &server_list, list) {
		lbuf_clear(rxbuf);

		int ret = curl_easy_perform(entry->handle);
		if (ret != 0) {
			log_print(LOG_ERROR, "p_apache: %s", curl_easy_strerror(ret));
			continue;
		}

		struct stats stats;
		memset(&stats, 0, sizeof(stats));

		char *line;
		while ((line = lbuf_gettok(rxbuf, "\r\n")) != NULL) {
			char *part[2];
			strsplit(line, ":", part, 2);

			if (!strcmp(part[0], "Total Accesses"))
				stats.total_accesses = atoll(part[1]);

			else if (!strcmp(part[0], "Total kBytes"))
				stats.total_kbytes = atoll(part[1]);

			else if (!strcmp(part[0], "BusyWorkers") || !strcmp(part[0], "BusyServers"))
				stats.busy_workers = atoll(part[1]);

			else if (!strcmp(part[0], "IdleWorkers") || !strcmp(part[0], "IdleServers"))
				stats.idle_workers = atoll(part[1]);

			lbuf_freetok(rxbuf);
		}

		char filename[32];
		int len = snprintf(filename, sizeof(filename), "apache-%s.rrd", entry->name);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, 0, "%llu:%llu:%llu:%llu",
				stats.total_accesses, stats.total_kbytes,
				stats.busy_workers, stats.idle_workers);
	}
	return 0;
}

static int init_cb(struct strtoken *tokens, void *privdata)
{
	if (tokens->count < 2) {
		log_print(LOG_ERROR, "p_apache: parse error");
		return -1;
	}

	struct server_entry *entry = malloc(sizeof(struct server_entry));
	if (entry == NULL) {
		log_print(LOG_ERROR, "p_apache: out of memory");
		return -1;
	}

	entry->name = strdup(tokens->field[0]);

	entry->handle = curl_easy_init();
	if (entry->handle == NULL) {
		free(entry);
		return -1;
	}

	/* set URL */
	int ret = curl_easy_setopt(entry->handle, CURLOPT_URL, tokens->field[1]);
	if (ret != 0) {
		log_print(LOG_ERROR, "p_apache: %s", curl_easy_strerror(ret));
		curl_easy_cleanup(entry->handle);
		free(entry);
		return -1;
	}

	if (tokens->field[2] != NULL && tokens->field[3] != NULL) {
		*(tokens->field[3] -1) = ':';

		log_print(LOG_INFO, "p_apache: auth: '%s'", tokens->field[2]);

		/* set username:password */
		ret = curl_easy_setopt(entry->handle, CURLOPT_USERPWD, tokens->field[2]);
		if (ret != 0) {
			log_print(LOG_ERROR, "p_apache: %s", curl_easy_strerror(ret));
			curl_easy_cleanup(entry->handle);
			free(entry);
			return -1;
		}
	}

	ret = curl_easy_setopt(entry->handle, CURLOPT_WRITEFUNCTION, &curl_callback);
	if (ret != 0) {
		log_print(LOG_ERROR, "p_apache: %s", curl_easy_strerror(ret));
		curl_easy_cleanup(entry->handle);
		free(entry);
		return -1;
	}

	ret = curl_easy_setopt(entry->handle, CURLOPT_WRITEDATA, entry);
	if (ret != 0) {
		log_print(LOG_ERROR, "p_apache: %s", curl_easy_strerror(ret));
		curl_easy_cleanup(entry->handle);
		free(entry);
		return -1;
	}

	log_print(LOG_INFO, "p_apache: added server '%s'", entry->name);
	list_add_tail(&entry->list, &server_list);
	return 0;
}


static int init(void)
{
	rxbuf = lbuf_create(BUFSIZE);
	if (rxbuf == NULL) {
		log_print(LOG_ERROR, "p_apache: out of memory");
		return -1;
	}

	config_get_strtokens("p_apache", "server", ",", 4, init_cb, NULL);
	return 0;
}

static int fini(void)
{
	struct server_entry *entry, *tmp;
	list_for_each_entry_safe(entry, tmp, &server_list, list) {
		curl_easy_cleanup(entry->handle);
		free(entry->name);
		free(entry);
	}

	lbuf_free(rxbuf);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "apache",
	.interval	= 60,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
