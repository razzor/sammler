/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helper.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define DS_CPU		1
#define DS_PROC		2

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *cpu_ds_def = {
	"DS:user:COUNTER:15:0:U "
	"DS:nice:COUNTER:15:0:U "
	"DS:system:COUNTER:15:0:U "
	"DS:idle:COUNTER:15:0:U "
	"DS:iowait:COUNTER:15:0:U "
	"DS:irq:COUNTER:15:0:U "
	"DS:softirq:COUNTER:15:0:U "
	"DS:steal:COUNTER:15:0:U "
};

static const char *proc_ds_def = {
	"DS:intr:COUNTER:15:0:U "
	"DS:ctxt:COUNTER:15:0:U "
	"DS:fork:COUNTER:15:0:U "
};

static const char * get_ds(int ds_id)
{
	switch (ds_id) {
	case DS_CPU:
		return cpu_ds_def;

	case DS_PROC:
		return proc_ds_def;

	default:
		return NULL;
	}
}

struct proc_ {
	unsigned long long intr;
	unsigned long long ctxt;
	unsigned long long fork;
};

static int probe(void)
{
	FILE *fp;
	struct proc_ proc;

	memset(&proc, 0, sizeof(proc));

	fp = fopen("/proc/stat", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin stat");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		if (!strncmp(buffer, "cpu", 3)) {
			char *val[9], filename[16];
			int numfields, len, cpu;

			if ((buffer[3] >= '0') && (buffer[3] <= '9')) {
				cpu = atoi(buffer +3);
				len = snprintf(filename, sizeof(filename), "cpu-%d.rrd", cpu);
				if (len < 0 || len >= sizeof(filename))
					continue;

			} else {
				strncpy(filename, "cpu.rrd", sizeof(filename));
			}

			numfields = strsplit(buffer, " \t\n", val, 9);
			if (numfields < 5)
				continue;

			else if (numfields == 5)
				val[5] = val[6] = val[7] = val[8] = "0";

			else if (numfields == 8)
				val[8] = "0";

			probe_submit(&plugin, filename, DS_CPU,
					"%s:%s:%s:%s:%s:%s:%s:%s",
					val[1], val[2], val[3], val[4],
					val[5], val[6], val[7], val[8]);

		} else if (!strncmp(buffer, "intr", 4)) {
			proc.intr = atoll(buffer + 5);

		} else if (!strncmp(buffer, "ctxt", 4)) {
			proc.ctxt = atoll(buffer + 5);

		} else if (!strncmp(buffer, "processes", 9)) {
			proc.fork = atoll(buffer + 10);
		}
	}

	probe_submit(&plugin, "proc.rrd", DS_PROC, "%llu:%llu:%llu",
			proc.intr, proc.ctxt, proc.fork);

	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "stat",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
