/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helper.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *ds_def = {
	"DS:byte_in:COUNTER:15:0:U "
	"DS:byte_out:COUNTER:15:0:U "
	"DS:pkt_in:COUNTER:15:0:U "
	"DS:pkt_out:COUNTER:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	FILE *fp;
	char *stats, *device;
	char *val[16], filename[32];
	int len;

	fp = fopen("/proc/net/dev", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin netdev");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {

		if (!(stats = strchr(buffer, ':')))
			continue;

		*stats++ = '\0';

		device = buffer;
		while (*device == ' ')
			device++;

		if (*device == '\0')
			continue;

		if (strsplit(stats, " \t\n", val, 16) != 16)
			continue;

		len = snprintf(filename, sizeof(filename), "net-%s.rrd", device);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, 0, "%s:%s:%s:%s",
				val[0], val[8], val[1], val[9]);
	}
	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "netdev",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
