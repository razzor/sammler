/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

struct sammler_plugin plugin;

static const char *ds_def = {
	"DS:1min:GAUGE:15:0:U "
	"DS:5min:GAUGE:15:0:U "
	"DS:15min:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	return ds_def;
}

static int probe(void)
{
	FILE *fp = fopen("/proc/loadavg", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin load: fopen()");
		return -1;
	}

	double load1, load5, load15;
	if (fscanf(fp, "%lf %lf %lf", &load1, &load5, &load15) != 3) {
		log_print(LOG_WARN, "plugin load: fscanf()");
		fclose(fp);
		return -1;
	}
	fclose(fp);

	probe_submit(&plugin, "load.rrd", 0, "%.02lf:%.02lf:%.02lf", load1, load5, load15);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "load",
	.interval	= 10,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
