/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "helper.h"
#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define DS_STAT		1
#define DS_GC		2

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *ds_def_stat = {
	"DS:in_hit:DERIVE:15:0:U "
	"DS:in_slow_tot:DERIVE:15:0:U "
	"DS:in_slow_mc:DERIVE:15:0:U "
	"DS:in_no_route:DERIVE:15:0:U "
	"DS:in_brd:DERIVE:15:0:U "
	"DS:in_martian_dst:DERIVE:15:0:U "
	"DS:in_martian_src:DERIVE:15:0:U "
	"DS:out_hit:DERIVE:15:0:U "
	"DS:out_slow_tot:DERIVE:15:0:U "
	"DS:out_slow_mc:DERIVE:15:0:U "
	"DS:in_hlist_search:DERIVE:15:0:U "
	"DS:out_hlist_search:DERIVE:15:0:U "
};

static const char *ds_def_gc = {
	"DS:entries:GAUGE:15:0:U "
	"DS:gc_total:DERIVE:15:0:U "
	"DS:gc_ignored:DERIVE:15:0:U "
	"DS:gc_goal_miss:DERIVE:15:0:U "
	"DS:gc_dst_overflow:DERIVE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	switch (ds_id) {
	case DS_STAT:
		return ds_def_stat;

	case DS_GC:
		return ds_def_gc;

	default:
		return NULL;
	}
}

static int probe(void)
{
	FILE *fp;
	char *val[17], filename[16];
	unsigned long long arr[17];
	int i, len, cpu = 0;

	fp = fopen("/proc/net/stat/rt_cache", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin rtstat");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		if (!strncmp(buffer, "entries", 7))
			continue;

		if (strsplit(buffer, " \t\n", val, 17) != 17)
			continue;

		for (i = 0; i < 17; i++)
			arr[i] = strtoll(val[i], NULL, 16);

		len = snprintf(filename, sizeof(filename), "rtstat-%d.rrd", cpu);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, DS_STAT,
				"%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu:%llu",
				arr[1], arr[2], arr[3], arr[4],
				arr[5], arr[6], arr[7], arr[8],
				arr[9], arr[10], arr[15], arr[16]);

		len = snprintf(filename, sizeof(filename), "rtcache-%d.rrd", cpu);
		if (len < 0 || len >= sizeof(filename))
			continue;

		probe_submit(&plugin, filename, DS_GC,
				"%llu:%llu:%llu:%llu:%llu",
				arr[0], arr[11], arr[12], arr[13], arr[14]);

		cpu++;
	}
	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "rtstat",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
