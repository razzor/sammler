/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "plugins.h"
#include "probe.h"

#define DS_MEMORY	1
#define DS_SWAP		2

#define BUFSIZE 1024

struct sammler_plugin plugin;
static char *buffer;

static const char *mem_ds_def = {
	"DS:total:GAUGE:15:0:U "
	"DS:free:GAUGE:15:0:U "
	"DS:buffers:GAUGE:15:0:U "
	"DS:cached:GAUGE:15:0:U "
};

static const char *swap_ds_def = {
	"DS:total:GAUGE:15:0:U "
	"DS:free:GAUGE:15:0:U "
};

static const char * get_ds(int ds_id)
{
	switch (ds_id) {
	case DS_MEMORY:
		return mem_ds_def;

	case DS_SWAP:
		return swap_ds_def;

	default:
		return NULL;
	}
}

struct meminfo_ {
	unsigned long long memtotal;
	unsigned long long memfree;
	unsigned long long buffers;
	unsigned long long cached;
	unsigned long long swaptotal;
	unsigned long long swapfree;
};

static int probe(void)
{
	FILE *fp;
	struct meminfo_ meminfo;

	memset(&meminfo, 0, sizeof(meminfo));

	fp = fopen("/proc/meminfo", "r");
	if (fp == NULL) {
		log_print(LOG_WARN, "plugin memory");
		return -1;
	}

	while (fgets(buffer, BUFSIZE, fp) != NULL) {
		if (!strncmp(buffer, "MemTotal:", 9))
			meminfo.memtotal = atoll(buffer + 10);

		else if (!strncmp(buffer, "MemFree:", 8))
			meminfo.memfree = atoll(buffer + 9);

		else if (!strncmp(buffer, "Buffers:", 8))
			meminfo.buffers = atoll(buffer + 9);

		else if (!strncmp(buffer, "Cached:", 7))
			meminfo.cached = atoll(buffer + 8);

		else if (!strncmp(buffer, "SwapTotal:", 10))
			meminfo.swaptotal = atoll(buffer + 11);

		else if (!strncmp(buffer, "SwapFree:", 9))
			meminfo.swapfree = atoll(buffer + 10);
	}

	probe_submit(&plugin, "memory.rrd", DS_MEMORY, "%llu:%llu:%llu:%llu",
			meminfo.memtotal, meminfo.memfree,
			meminfo.buffers, meminfo.cached);

	probe_submit(&plugin, "swap.rrd", DS_SWAP, "%llu:%llu",
			meminfo.swaptotal, meminfo.swapfree);

	fclose(fp);
	return 0;
}

static int init(void)
{
	buffer = malloc(BUFSIZE);
	return (buffer == NULL) ? -1 : 0;
}

static int fini(void)
{
	free(buffer);
	return 0;
}

struct sammler_plugin plugin = {
	.name		= "memory",
	.interval	= 10,
	.init		= &init,
	.fini		= &fini,
	.probe		= &probe,
	.get_ds		= &get_ds,
};
