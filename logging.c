/***************************************************************************
 *   Copyright (C) 07/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>

#include "logging.h"

#define BUFSIZE 8192

static FILE *log_file = NULL;
static int log_prio = LOG_EVERYTIME;
static char *buffer = NULL;

int log_print(int prio, const char *fmt, ...)
{
	va_list az;
	int len = 0, retval;

	if (prio < log_prio)
		return 0;

	if (buffer == NULL) {
		buffer = malloc(BUFSIZE);
		if (buffer == NULL) {
			fprintf(stderr, "%s(): out of memory\n", __FUNCTION__);
			return -1;
		}
	}

	if (log_file != NULL) {
		time_t  tzgr;
		time(&tzgr);

		len += strftime(buffer, BUFSIZE, "%b %d %H:%M:%S :", localtime(&tzgr));
	}

	va_start(az, fmt);
	len += vsnprintf(buffer + len, BUFSIZE - len, fmt, az);
	va_end(az);

	if (len < 0 || len >= BUFSIZE) {
		errno = 0;
		return log_print(LOG_ERROR, "%s: arguments too long", __FUNCTION__);
	}

	if (errno) {
		len += snprintf(buffer + len, BUFSIZE - len, ": %s", strerror(errno));
		errno = 0;
	}

	retval = fprintf((log_file ? log_file : stderr), "%s\n", buffer);
	fflush(log_file);
	return retval;
}

void log_close(void)
{
	if (buffer) {
		free(buffer);
		buffer = NULL;
	}

	if (log_file) {
		fclose(log_file);
		log_file = NULL;
	}
}

int log_init(const char *logfile)
{
	if (log_file != NULL)
		log_close();

	log_file = fopen(logfile, "a");
	if (log_file == NULL) {
		fprintf(stderr, "%s(): can not open logfile", __FUNCTION__);
		return -1;
	}

	if (fcntl(fileno(log_file), F_SETFD, FD_CLOEXEC) < 0) {
		fprintf(stderr, "%s(): fcntl(FD_CLOEXEC)", __FUNCTION__);
		fclose(log_file);
		return -1;
	}

	log_prio = LOG_EVERYTIME;
	return 0;
}

void log_setprio(int prio)
{
	log_prio = prio;
}
