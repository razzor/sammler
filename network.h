#ifndef _NETWORK_H_
#define _NETWORK_H_

#include "plugins.h"

int net_init(void);
void net_close(void);

int net_submit(const char *hostname, const char *pluginname, const char *filename, int ds_id, const char *data);
void net_submit_flush(void);

#endif /* _NETWORK_H_ */
