/***************************************************************************
 *   Copyright (C) 06/2006 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <string.h>
#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#include "configfile.h"
#include "event.h"
#include "logging.h"
#include "network.h"
#include "plugins.h"

#define FLAGS_ACTIVE	0x01

static LIST_HEAD(plugin_list);
static struct event_timeout *probe_event;

static int plugin_init_cb(const char *filename, void *privdata)
{
	const char *plugin_dir = (const char *)privdata;

	char *fullname = malloc(PATH_MAX);
	if (fullname == NULL) {
		log_print(LOG_ERROR, "plugin_init_cb(): out of memory");
		return -1;
	}

	int len = snprintf(fullname, PATH_MAX, "%s/%s", plugin_dir, filename);
	if (len < 0 || len >= PATH_MAX) {
		log_print(LOG_ERROR, "plugin_init_cb(): file name too long: %s/%s", plugin_dir, filename);
		free(fullname);
		return -1;
	}

	dlerror();
	void *dlhandle = dlopen(fullname, RTLD_NOW);
	if (dlhandle == NULL) {
		log_print(LOG_ERROR, "plugin_init_cb(): dlopen: %s", dlerror());
		free(fullname);
		return -1;
	}

	free(fullname);

	struct sammler_plugin *plugin = dlsym(dlhandle, "plugin");
	if (plugin == NULL) {
		log_print(LOG_ERROR, "plugin_init_cb(): failed to load '%s'", filename);
		dlclose(dlhandle);
		return -1;
	}

	if (plugin->init != NULL && (plugin->init() != 0)) {
		log_print(LOG_ERROR, "plugin_init_cb(): Plugin '%s': init() failed", plugin->name);
		dlclose(dlhandle);
		return -1;
	}

	log_print(LOG_INFO, "Plugin '%s' loaded", plugin->name);
	plugin->lastprobe = 0;
	plugin->flags = FLAGS_ACTIVE;
	plugin->dlhandle = dlhandle;

	list_add_tail(&plugin->list, &plugin_list);
	return 0;
}

static int plugins_probe(int timerid, void *privdata)
{
	time_t now;
	time(&now);

	struct sammler_plugin *plugin;
	list_for_each_entry(plugin, &plugin_list, list) {
		if (!(plugin->flags & FLAGS_ACTIVE))
			continue;

		if (plugin->lastprobe + plugin->interval <= now) {
			if (plugin->probe() != 0) {
				log_print(LOG_ERROR, "plugin_probe(): plugin '%s' disabled", plugin->name);
				plugin->flags &= ~FLAGS_ACTIVE;
			}
			plugin->lastprobe = now;
		}
	}

	net_submit_flush();
	return 0;
}

int plugin_init(void)
{
	const char *plugin_dir = config_get_string("global", "plugin_dir", "plugins");
	int cnt = config_get_strings("global", "plugin", plugin_init_cb, (void *)plugin_dir);
	if (cnt == 0) {
		log_print(LOG_ERROR, "plugin_init(): no working plugins");
		return -1;
	}

	probe_event = event_add_timeout_ms(1000, plugins_probe, 0, NULL);

	return 0;
}

int plugin_close(void)
{
	struct sammler_plugin *plugin, *tmp;
	list_for_each_entry_safe(plugin, tmp, &plugin_list, list) {
		list_del(&plugin->list);
		if (plugin->fini != NULL && plugin->fini() != 0)
			log_print(LOG_ERROR, "plugin_close(): Plugin '%s': fini() failed", plugin->name);

		dlclose(plugin->dlhandle);
	}

	event_remove_timeout(probe_event);
	return 0;
}

struct sammler_plugin * plugin_lookup(const char *name)
{
	struct sammler_plugin *plugin;
	list_for_each_entry(plugin, &plugin_list, list) {
		if (!strcmp(plugin->name, name))
			return plugin;
	}
	return NULL;
}
