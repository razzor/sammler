#ifndef _PROBE_H_
#define _PROBE_H_

#include "plugins.h"

int probe_init(void);
int probe_submit(struct sammler_plugin *plugin, const char *filename, int ds_id, const char *fmt, ... );

#endif /* _PROBE_H_ */
