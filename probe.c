#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

#include "configfile.h"
#include "logging.h"
#include "network.h"
#include "rrdtool.h"

#define BUFSIZE 512

#define SUBMIT_NET_ONLY	0x01

static int submit_flags;
static const char *hostname;

int probe_init(void)
{
	const char *fwd_only = config_get_string("global", "forward_only", "false");
	submit_flags = 0;

	if (!strncmp(fwd_only, "true", 4))
		submit_flags |= SUBMIT_NET_ONLY;

	static char hostname_buf[32];
	if (gethostname(hostname_buf, sizeof(hostname_buf)) != 0)
		strcpy(hostname_buf, "localhost");

	hostname = config_get_string("global", "hostname", hostname_buf);
	return 0;
}

int probe_submit(struct sammler_plugin *plugin, const char *filename, int ds_id, const char *fmt, ... )
{
	char *buffer = malloc(BUFSIZE);
	if (buffer == NULL) {
		log_print(LOG_ERROR, "probe_submit: out of memory");
		return -1;
	}

	va_list az;
	va_start(az, fmt);
	int len = vsnprintf(buffer, BUFSIZE, fmt, az);
	va_end(az);

	if (len < 0 || len >= BUFSIZE) {
		log_print(LOG_ERROR, "probe_submit: %s arguments too long", plugin->name);
		free(buffer);
		return -1;
	}

	net_submit(hostname, plugin->name, filename, ds_id, buffer);

	if (!(submit_flags & SUBMIT_NET_ONLY))
		rrd_submit(hostname, plugin->name, filename, ds_id, buffer);

	free(buffer);
	return 0;
}
