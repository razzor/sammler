#ifndef _PLUGINS_H_
#define _PLUGINS_H_

#include <time.h>
#include "list.h"

struct sammler_plugin {
	struct list_head list;
	void *dlhandle;
	const char *name;

	/* timing */
	unsigned int interval;
	time_t lastprobe;

	/* flags */
	int flags;

	int (*init) (void);
	int (*fini) (void);
	int (*probe) (void);
	const char * (*get_ds) (int ds_id);
};

int plugin_init(void);
int plugin_close(void);

struct sammler_plugin * plugin_lookup(const char *name);

#endif /* _PLUGINS_H_ */
