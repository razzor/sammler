/***************************************************************************
 *   Copyright (C) 07/2007 by Olaf Rempel                                  *
 *   razzor@kopf-tisch.de                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; version 2 of the License                *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int parse_sockaddr(const char *addr, struct sockaddr_in *sa)
{
	char *buf = strdup(addr);
	if (buf == NULL)
		return -1;

	char *tmp;
	char *ipstr = strtok_r(buf, ":", &tmp);
	if (ipstr == NULL) {
		free(buf);
		return -2;
	}

	sa->sin_family = AF_INET;
	if (inet_pton(AF_INET, ipstr, &sa->sin_addr) <= 0) {
		free(buf);
		return -3;
	}

	char *portstr = strtok_r(NULL, " \r\n", &tmp);
	if (portstr == NULL) {
		free(buf);
		return -4;
	}

	int port = atoi(portstr);
	if (port < 0 || port > 65535) {
		free(buf);
		return -5;
	}

	sa->sin_port = htons(port);
	free(buf);
	return 0;
}

int parse_subnet(const char *addr, struct in_addr *net, struct in_addr *mask)
{
	char *buf = strdup(addr);
	if (buf == NULL)
		return -1;

	char *tmp;
	char *netstr = strtok_r(buf, "/", &tmp);
	if (netstr == NULL) {
		free(buf);
		return -2;
	}

	if (inet_pton(AF_INET, netstr, net) <= 0) {
		free(buf);
		return -3;
	}

	char *maskstr = strtok_r(NULL, " \r\n", &tmp);
	if (maskstr == NULL) {
		mask->s_addr = ~0;

	} else if (inet_pton(AF_INET, maskstr, mask) <= 0) {
		int maskbits = atoi(maskstr);
		if (maskbits < 0 || maskbits > 32)  {
			free(buf);
			return -4;
		}

		mask->s_addr = htonl(~0 << (32 - maskbits));
	}

	free(buf);
	return 0;
}

int get_sockaddr(char *buf, int size, struct sockaddr_in *addr)
{
	return snprintf(buf, size, "%s:%d", inet_ntoa(addr->sin_addr), ntohs(addr->sin_port));
}

char * get_sockaddr_buf(struct sockaddr_in *addr)
{
	static char ret[24];
	get_sockaddr(ret, sizeof(ret), addr);
	return ret;
}

int same_sockaddr(struct sockaddr_in *a, struct sockaddr_in *b)
{
    return !((a->sin_family ^ b->sin_family) |
	    (a->sin_addr.s_addr ^ b->sin_addr.s_addr) |
	    (a->sin_port ^ b->sin_port));
}
