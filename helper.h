#ifndef _HELPER_H_
#define _HELPER_H_

#define DFLT_DELIM	" \n\t"

int strsplit(char *string, char *delim, char **fields, int size);

#endif /* _HELPER_H_ */
