#ifndef _RRDTOOL_H_
#define _RRDTOOL_H_

int sammler_rrd_init(void);
int rrd_submit(const char *hostname, const char *pluginname, const char *filename, int ds_id, const char *data);

#endif /* _RRDTOOL_H_ */
