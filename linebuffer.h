#ifndef _LINEBUFFER_H_
#define _LINEBUFFER_H_

#include <stdarg.h>

/* hide details */
struct lbuf;

struct lbuf * lbuf_create(size_t size);
void lbuf_free(struct lbuf *buf);

int lbuf_clear(struct lbuf *buf);

int lbuf_readfd(struct lbuf *buf, int fd);
int lbuf_parsefd(struct lbuf *buf, int fd);
int lbuf_writefd(struct lbuf *buf, int fd);

char * lbuf_getdata(struct lbuf *buf, size_t *len);

int lbuf_append(struct lbuf *buf, const char *src, size_t size);
int lbuf_vprintf(struct lbuf *buf, const char *fmt, va_list ap);
int lbuf_printf(struct lbuf *buf, const char *fmt, ...);

char * lbuf_gettok(struct lbuf *buf, const char *delim);
int lbuf_freetok(struct lbuf *buf);

#endif /* _LINEBUFFER_H_ */
